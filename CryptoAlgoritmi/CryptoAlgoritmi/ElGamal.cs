﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;

namespace CryptoAlgoritmi
{
    public class ElGamal
    {
        protected int x;
        protected int p;
        protected int g;
        protected int y;

        int P { get { return p; } }
        int G { get { return g; } }
        int Y { get { return y; } }


        public ElGamal()
        {
            Random rand = new Random();
            x = rand.Next(15);
           // GenerateKey();
        }


        public int RandomPrime()
        {
            int a = 0;
            Random rand = new Random();
            bool prime = false;
            while(prime == false)
            {
                a = rand.Next(20);
                prime = true;

                for(int i = 2; i <= a/2; i++)
                {
                    if (a % i == 0)
                    {
                        prime = false;
                        break;
                    }
                }
            }
            return a;
        }

        public void GenerateKey()
        {
            Random rand = new Random();
           
            p = RandomPrime();
            g = rand.Next(p-1);
            y = Convert.ToInt32(Math.Pow(g, x)) % p;
        }



        public int[] Encrypt(int m, int k)
        {
            int[] res = new int[2];
            int a = Convert.ToInt32(Math.Pow(g, k)) % p;
            int b = Convert.ToInt32(Math.Pow(y, k))*m % p;
            res[0] = a;
            res[1] = b;
            return res;
        }

        public int Decrypt(int a, int b)
        {
            int m = (b / Convert.ToInt32(Math.Pow(a, x))) % p;
            return m;
        }
    }
}
