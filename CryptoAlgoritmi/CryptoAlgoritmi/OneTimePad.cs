﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryptoAlgoritmi
{
    public class OneTimePad
    {
        Dictionary<char, byte> binCode;
        Dictionary<byte, char> charCode;

        public OneTimePad()
        {
            binCode = new Dictionary<char, byte>();
            charCode = new Dictionary<byte, char>();
            FillDictionary();
        }

        public void FillDictionary()
        {
            char ascii = 'A';
            byte b = 0;


            int end = System.Convert.ToInt32('z');

            for (int a = System.Convert.ToInt32(ascii); a <= end; a++)
            {
                char asc = System.Convert.ToChar(a);
                binCode.Add(asc, b);
                charCode.Add(b, asc);
                b++;
            }

        }

        public byte[] GenerateBytes(String data, int size)
        {
            byte[] result = new byte[size];
            int i = 0;

            foreach (char c in data)
            {
                byte b = binCode[c];
                result[i] = b;
                i++;
            }

            return result;
        }

        public String GenerateText(byte[] data)
        {
            String result = "";


            foreach (byte b in data)
            {
                result += charCode[b];
            }

            return result;
        }

        public byte[] Encrypt(byte[] data, byte[] pad, int size)
        {
            var result = new byte[size];
            for (int i = 0; i < data.Length; i++)
            {
                result[i] = (byte)(data[i] ^ pad[i]);
            }
            return result;
        }


        public byte[] Decrypt(byte[] encrypted, byte[] pad, int size)
        {
            var result = new byte[size];
            for (int i = 0; i < encrypted.Length; i++)
            {
                result[i] = (byte)(encrypted[i] ^ pad[i]);
            }
            return result;
        }





    }


}
