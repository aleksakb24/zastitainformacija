﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryptoAlgoritmi
{
    public class SHA1
    {

        public byte[] padInput(byte[] input)
        {
            uint bytesToPad;

            if (input.Length % 64 == 0)
                bytesToPad = 64;
            else if (((64 - (input.Length % 64)) % 64) > 7)
                bytesToPad = Convert.ToUInt32((64 - (input.Length % 64)) % 64);
            else
                bytesToPad = Convert.ToUInt32((64 - (input.Length % 64)) % 64 + 64);

            byte[] paddedInput = new byte[input.Length + bytesToPad];


            for (int i = 0; i < input.Length; i++)
            {
                paddedInput[i] = input[i];
            }
            paddedInput[input.Length] = 0x80;

            for (int i = 1; i < bytesToPad - 8; i++)
            {
                paddedInput[input.Length + i] = 0;
            }

            byte[] len = BitConverter.GetBytes(Convert.ToUInt64(input.Length));
            for (int i = 0; i < 8; i++)
            {
                paddedInput[input.Length + bytesToPad - 8 + i] = len[i];
            }

            return paddedInput;
        }




        public void processBlock(uint[] block, uint[] hash, uint[] bigarray)
        {
            uint temp;
            uint h0;
            uint h1;
            uint h2;
            uint h3;
            uint h4;
            int t = block.Length;
            int i;



            for (int s = 0; s < t; s += 64)
            {
                temp = 0;
                h0 = hash[0];
                h1 = hash[1];
                h2 = hash[2];
                h3 = hash[3];
                h4 = hash[4];


                for (i = 0; i < 16; i++)
                {
                    bigarray[i] = block[i];
                }

                for (i = 16; i < 80; i++)
                {
                    bigarray[i] = circularShift(1, (bigarray[i - 3] ^ bigarray[i - 8] ^ bigarray[i - 14] ^ bigarray[i - 16]));
                }

                uint A = h0;
                uint B = h1;
                uint C = h2;
                uint D = h3;
                uint E = h4;

                uint f = 0;
                uint k = 0;

                for (i = 0; i < 80; i++)
                {
                    if (i < 20)
                    {
                        f = (B & C) | ((~B) & D);
                        k = 0x5A827999;
                    }
                    else if (i < 40)
                    {
                        f = B ^ C ^ D;
                        k = 0x6ED9EBA1;
                    }
                    else if (i < 60)
                    {
                        f = (B & C) | (B & D) | (C & D);
                        k = 0x8F1BBCDC;
                    }
                    else if (i < 80)
                    {
                        f = B ^ C ^ D;
                        k = 0xCA62C1D6;
                    }

                    temp = circularShift(5, A) + f + E + k + bigarray[i];
                    E = D;
                    D = C;
                    C = circularShift(30, B);
                    B = A;
                    A = temp;


                }


                hash[0] += A;
                hash[1] += B;
                hash[2] += C;
                hash[3] += D;
                hash[4] += E;
            }

        }

        public uint circularShift(int bits, uint word)
        {
            uint output = (word << bits) | (word >> (32 - bits));
            return output;
        }

        public string byteArrayToString(byte[] input)
        {
            string tempst = "";

            for (int i = 0; i < input.Length; i++)
                tempst += input[i].ToString("X2");
            return tempst;
        }

        public string uintArrayToString(uint[] input)
        {
            string result = "";
            for (int i = 0; i < input.Length; i++)
            {
                uint high = input[i] >> 24;
                uint midhigh = (input[i] << 8) >> 24;
                uint midlow = (input[i] << 16) >> 24;
                uint low = (input[i] << 24) >> 24;
                result += high.ToString("X2") + midhigh.ToString("X2") + midlow.ToString("X2") + low.ToString("X2");
            }
            return result;
        }



    }
}
