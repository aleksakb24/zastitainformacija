﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryptoAlgoritmi
{
    public class Tea
    {

        public uint[] Encrypt(uint[] v, uint[] k)
        {
            uint v0 = v[0];
            uint v1 = v[1];
            uint sum = 0;
            uint i;

            uint delta = 0x9e3779b9;

            uint k0 = k[0];
            uint k1 = k[1];
            uint k2 = k[2];
            uint k3 = k[3];

            for(i = 0; i < 32; i++)
            {
                sum += delta;
                v0 += ((v1 << 4) + k0) ^ (v1 + sum) ^ ((v1 >> 5) + k1);
                v1 += ((v0 << 4) + k2) ^ (v0 + sum) ^ ((v0 >> 5) + k3);
            }

            v[0] = v0;
            v[1] = v1;

            return v;

        }

        public uint[] Decrypt(uint[] v, uint[] k)
        {
            uint v0 = v[0];
            uint v1 = v[1];
            uint sum = 0xC6EF3720;
            uint i;

            uint delta = 0x9e3779b9;

            uint k0 = k[0];
            uint k1 = k[1];
            uint k2 = k[2];
            uint k3 = k[3];

            for (i = 0; i < 32; i++)
            {
                v1 -= ((v0 << 4) + k2) ^ (v0 + sum) ^ ((v0 >> 5) + k3);
                v0 -= ((v1 << 4) + k0) ^ (v1 + sum) ^ ((v1 >> 5) + k1);
                sum -= delta;
            }

            v[0] = v0;
            v[1] = v1;

            return v;
        }


        public string uintArrayToString(uint[] input)
        {
            string result = "";
            for (int i = 0; i < input.Length; i++)
            {
                uint high = input[i] >> 24;
                uint midhigh = (input[i] << 8) >> 24;
                uint midlow = (input[i] << 16) >> 24;
                uint low = (input[i] << 24) >> 24;
                result += high.ToString("X2") + midhigh.ToString("X2") + midlow.ToString("X2") + low.ToString("X2");
            }
            return result;
        }

    }
}
