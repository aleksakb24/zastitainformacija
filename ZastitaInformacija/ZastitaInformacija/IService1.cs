﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace ZastitaInformacija
{
    
    [ServiceContract]
    public interface IService1
    {
        [OperationContract]
        bool isFull(String user);

        [OperationContract]
        void otpUpload(String user, String fileName, String Crypto, String encryptedText, String padText, int size, String hashCode);

        [OperationContract]
        void otpDownload(String user, String file, String decryptedText);

        [OperationContract]
        void otpAfterDEncrypt(String user, String file, String encryptedText);

        [OperationContract]
        void teaUpload(String user, String fileName, String Crypto, String encryptedTextA, String encryptedTextB, String padText, int size, String hashCode);

        [OperationContract]
        void teaDownload(String user, String file, String decryptedText);

        [OperationContract]
        void teaAfterDEncrypt(String user, String file, String encryptedText);

        [OperationContract]
        void elGamalUpload(String user, String fileName, String Crypto, String a, String b, int size, String key, String hashCode);

        [OperationContract]
        void elGamalDownload(String user, String file, String decryptedText);

        [OperationContract]
        void elGamalAfterDEncrypt(String user, String file, int a, int b);

    }
}
