﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Web.Hosting;

namespace ZastitaInformacija
{
    
    public class Service1 : IService1
    {
        public bool isFull(String user)
        {
            int maxFiles = 10;
            int currentFiles = 0;

            foreach (string strFile in Directory.GetFiles(HostingEnvironment.MapPath("~/Data/" + user)))
            {
                currentFiles++;
            }

            if (currentFiles == maxFiles)
                return true;
            else 
                return false;

        }


        public void otpUpload(string user, string fileName, string Crypto, string encryptedText, string padText, int size, String hashCode)
        {
            StreamWriter writeToData = new StreamWriter(HostingEnvironment.MapPath("~/Data/" + user + "/") + fileName);

            writeToData.Write(encryptedText);
            writeToData.Close();

            StreamWriter writeToUser = new StreamWriter(HostingEnvironment.MapPath("~/Users/" + user + ".txt"), append: true);
            writeToUser.WriteLine();
            writeToUser.WriteLine(fileName);
            writeToUser.WriteLine(Crypto);
            writeToUser.WriteLine(padText);
            writeToUser.WriteLine(size);
            writeToUser.WriteLine(hashCode);
            writeToUser.Close();
        }

        public void otpDownload(String user, String file, String decryptedText)
        {
            StreamWriter writeToData = new StreamWriter(HostingEnvironment.MapPath("~/Data/" + user + "/") + file);
            writeToData.Write(decryptedText);
            writeToData.Close();
        }

        public void otpAfterDEncrypt(String user, String file, String encryptedText)
        {
            StreamWriter writeToData2 = new StreamWriter(HostingEnvironment.MapPath("~/Data/" + user + "/") + file);
            writeToData2.Write(encryptedText);
            writeToData2.Close();
        }

        public void teaUpload(string user, string fileName, string Crypto, string encryptedTextA, String encryptedTextB, string key, int size, String hashCode)
        {
            StreamWriter writeToData = new StreamWriter(HostingEnvironment.MapPath("~/Data/" + user + "/") + fileName);

            writeToData.WriteLine(encryptedTextA);
            writeToData.WriteLine(encryptedTextB);
            writeToData.Close();

            StreamWriter writeToUser = new StreamWriter(HostingEnvironment.MapPath("~/Users/" + user + ".txt"), append: true);
            writeToUser.WriteLine();
            writeToUser.WriteLine(fileName);
            writeToUser.WriteLine(Crypto);
            writeToUser.WriteLine(key);
            writeToUser.WriteLine(size);
            writeToUser.WriteLine(hashCode);
            writeToUser.Close();
        }

        public void teaDownload(String user, String file, String decryptedText)
        {
            StreamWriter writeToData = new StreamWriter(HostingEnvironment.MapPath("~/Data/" + user + "/") + file);
            writeToData.Write(decryptedText);
            writeToData.Close();
        }

        public void teaAfterDEncrypt(string user, string file, string encryptedText)
        {
            StreamWriter writeToData2 = new StreamWriter(HostingEnvironment.MapPath("~/Data/" + user + "/") + file);
            writeToData2.Write(encryptedText);
            writeToData2.Close();
        }

        public void elGamalUpload(string user, string fileName, string Crypto, string a, string b, int size, string key, string hashCode)
        {
            StreamWriter writeToData = new StreamWriter(HostingEnvironment.MapPath("~/Data/" + user + "/") + fileName);

            writeToData.WriteLine(a);
            writeToData.WriteLine(b);
            writeToData.Close();

            StreamWriter writeToUser = new StreamWriter(HostingEnvironment.MapPath("~/Users/" + user + ".txt"), append: true);
            writeToUser.WriteLine();
            writeToUser.WriteLine(fileName);
            writeToUser.WriteLine(Crypto);
            writeToUser.WriteLine(key);
            writeToUser.WriteLine(size);
            writeToUser.WriteLine(hashCode);
            writeToUser.Close();
        }

        public void elGamalDownload(string user, string file, string decryptedText)
        {
            StreamWriter writeToData = new StreamWriter(HostingEnvironment.MapPath("~/Data/" + user + "/") + file);
            writeToData.Write(decryptedText);
            writeToData.Close();
        }

        public void elGamalAfterDEncrypt(string user, string file, int a, int b)
        {
            StreamWriter writeToData2 = new StreamWriter(HostingEnvironment.MapPath("~/Data/" + user + "/") + file);
            writeToData2.WriteLine(a);
            writeToData2.WriteLine(b);
            writeToData2.Close();
        }
    }
}
