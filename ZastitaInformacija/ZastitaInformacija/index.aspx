﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="ZastitaInformacija.index"%>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="index.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">

        <div style="height: 50px">
            <asp:Label ID="lblUser" runat="server" Font-Bold="True" Font-Size="X-Large" Text="Label" CssClass="borderName"></asp:Label>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Button ID="btnLogout" runat="server" OnClick="btnLogout_Click" Text="Sign Out" CssClass="buttonSignOut"/>
        </div>

        <div style="font-family:Arial; height: 53px;">
            <asp:FileUpload ID="FileUpload1" runat="server" class="upload"/>
            &nbsp;
            </div>

        <div style="height: 43px">

            <asp:RadioButton ID="rbtnOtp" GroupName="crypto" runat="server" value="otp" Text="One Time Pad" />
&nbsp;&nbsp;
            <asp:RadioButton ID="rbtnTea" GroupName="crypto" runat="server" value="tea" Text="TEA" />
&nbsp;&nbsp;
            <asp:RadioButton ID="rbtnElGamal" GroupName="crypto" runat="server" value="elgamal" Text="El Gamal" />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Label ID="Label1" runat="server" Text="Crypto Code:" CssClass="textBox"></asp:Label>
&nbsp;<asp:TextBox ID="txtCryptoCode" runat="server"></asp:TextBox>

        </div>

        <div style="height: 183px" class="divTable">
                <asp:GridView ID="GridView1" runat="server" CellPadding="4" GridLines="Horizontal" AutoGenerateColumns="False" OnRowCommand="GridView1_RowCommand" BackColor="White" BorderColor="#336666" BorderStyle="Double" BorderWidth="3px">
                <Columns>
                    <asp:TemplateField HeaderText="File">
                        <ItemTemplate>
                            <asp:LinkButton ID="LinkButton1" runat="server" CommandArgument='<%# Eval("file") %>' CommandName="Download" Text='<%# Eval("file") %>'></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="Size" HeaderText="Size in Bytes" />
                    <asp:BoundField DataField="Type" HeaderText="File Type" />
                </Columns>
                <FooterStyle BackColor="White" ForeColor="#333333" />
                <HeaderStyle BackColor="#336666" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Center" />
                <RowStyle BackColor="White" ForeColor="#333333" />
                <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                <SortedAscendingCellStyle BackColor="#F7F7F7" />
                <SortedAscendingHeaderStyle BackColor="#487575" />
                <SortedDescendingCellStyle BackColor="#E5E5E5" />
                <SortedDescendingHeaderStyle BackColor="#275353" />
            </asp:GridView>
                </div>
                <div class="divSignout">
            <asp:Button ID="btnUpload" runat="server" Text="Upload" OnClick="btnUpload_Click" CssClass="buttonUpload"/>
        </div>

    </form>
</body>
</html>
