﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using ZastitaInformacija.ZastitaService;
using System.Text;
using CryptoAlgoritmi;


namespace ZastitaInformacija
{
    public partial class index : System.Web.UI.Page
    {
        Service1 zService = new Service1(); 
        OneTimePad otp = new OneTimePad();
        Tea tea = new Tea();
        ElGamal elGamal = new ElGamal();
        SHA1 sha1 = new SHA1();

        protected void Page_Load(object sender, EventArgs e)
        {
            string user = Request.QueryString["user"].ToString();
            lblUser.Text = user + "'s Drive";
            ShowTable(user);

            if (!Page.IsPostBack)
            {
                ShowTable(user);
            }

        }

        private void ShowTable(string user)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("File", typeof(string));
            dt.Columns.Add("Size", typeof(string));
            dt.Columns.Add("Type", typeof(string));

            foreach (string strFile in Directory.GetFiles(Server.MapPath("~/Data/" + user)))
            {
                FileInfo fi = new FileInfo(strFile);

                dt.Rows.Add(fi.Name, fi.Length, fi.Extension);
            }

            GridView1.DataSource = dt;
            GridView1.DataBind();
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            string user = Request.QueryString["user"].ToString();

            if (!zService.isFull(user))
            {
                if (FileUpload1.HasFile)
                {
                    if (rbtnOtp.Checked)
                        otpUpload(user);
                    else if (rbtnTea.Checked)
                        teaUpload(user);
                    else if (rbtnElGamal.Checked)
                        elGamalUpload(user);
                }
            }
            else
                Response.Write("<script>alert('Storage full.')</script>");

            ShowTable(user);
        }


        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
           

            if (e.CommandName == "Download")
            {

                string user = Request.QueryString["user"].ToString();
                string file = e.CommandArgument.ToString();

                StreamReader readUser = new StreamReader(Server.MapPath("~/Users/" + user + ".txt"));
                while (readUser.ReadLine() != file)
                { }
                
                string type = readUser.ReadLine();               
                string key = readUser.ReadLine();
                int size = Convert.ToInt32(readUser.ReadLine());
                string hashCode = readUser.ReadLine();

                readUser.Close();

                if (type == "otp")
                    otpDownload(user, file, type, key, size, e, hashCode);
                else if (type == "tea")
                    teaDownload(user, file, type, key, size, e, hashCode);
                //else if (type == "elgamal")
                 //   elGamalDownload(user, file, type, key, size, e, hashCode);
               
            }
            
        
        }

      

        protected void otpUpload(String user)
        {
            StreamReader readUpFile = new StreamReader(FileUpload1.FileContent);

            int size = FileUpload1.PostedFile.ContentLength;
            String fileContent = readUpFile.ReadToEnd();
            byte[] dataBytes = otp.GenerateBytes(fileContent, size);
            byte[] padBytes = otp.GenerateBytes(txtCryptoCode.Text, size);
            byte[] encrypted = otp.Encrypt(dataBytes, padBytes, size);
            String hashCode = HashCode(fileContent);

            readUpFile.Close();

            String fileName = FileUpload1.FileName;
            String encryptedText = otp.GenerateText(encrypted);
            String padText = otp.GenerateText(padBytes);

            FileUpload1.PostedFile.SaveAs(Server.MapPath("~/Data/" + user + "/") + fileName);

            zService.otpUpload(user,fileName,"otp",encryptedText,padText,size,hashCode);

        }
        protected void otpDownload(String user, String file, String type, String key, int size, GridViewCommandEventArgs e, String hashCode)
        {
            StreamReader readData = new StreamReader(Server.MapPath("~/Data/" + user + "/") + file);
            string encrypted = readData.ReadLine();
            readData.Close();

            byte[] dataBytes = otp.GenerateBytes(encrypted, size);
            byte[] padBytes = otp.GenerateBytes(key, size);
            byte[] decrypted = otp.Decrypt(dataBytes, padBytes, size);

            String decryptedText = otp.GenerateText(decrypted);
            String hashCode2 = HashCode(decryptedText);
            if (hashCode == hashCode2)
            {
                zService.otpDownload(user, file, decryptedText);
                DownloadFile(user, e);
                zService.otpAfterDEncrypt(user, file, encrypted);
            }
            else
                Response.Write("<script>alert('Wrong hash.')</script>");

        }

        public uint[] GenerateTeaData(String data)
        {
            uint[] dataU = new uint[2];
            byte[] dataB = new byte[8];

            for (int i = 0; i < dataU.Length; i++)
                dataU[i] = 0;

            for (int i = 0; i < dataB.Length; i++)
                dataB[i] = 0;
     
            byte[] dataBytes = System.Text.Encoding.UTF8.GetBytes(data);
            Array.Reverse(dataBytes);
            Array.Copy(dataBytes, dataB, (dataBytes.Length > 8)? 8 : dataBytes.Length);
            dataU[0] = BitConverter.ToUInt32(dataB, 0);
            dataU[1] = BitConverter.ToUInt32(dataB, 4);

            return dataU;
        }

        public uint[] GenerateTeaKey(String key)
        {
            uint[] keyU = new uint[4];
            byte[] keyB = new byte[16];

            for (int i = 0; i < keyU.Length; i++)
                keyU[i] = 0;

            for (int i = 0; i < keyB.Length; i++)
                keyB[i] = 0;

            byte[] keyBytes = System.Text.Encoding.UTF8.GetBytes(key);
            Array.Reverse(keyBytes);
            Array.Copy(keyBytes, keyB, (keyBytes.Length > 8) ? 8 : keyBytes.Length);
            keyU[0] = BitConverter.ToUInt32(keyB, 0);
            keyU[1] = BitConverter.ToUInt32(keyB, 4);
            keyU[2] = BitConverter.ToUInt32(keyB, 8);
            keyU[3] = BitConverter.ToUInt32(keyB, 12);

            return keyU;
        }

        protected void teaUpload(String user)
        {
            int size = FileUpload1.PostedFile.ContentLength;
            StreamReader readUpFile = new StreamReader(FileUpload1.FileContent);
            string data = readUpFile.ReadToEnd();
            String hashCode = HashCode(data);
            readUpFile.Close();

            FileUpload1.PostedFile.SaveAs(Server.MapPath("~/Data/" + user + "/") + FileUpload1.FileName);

            String fileName = FileUpload1.FileName;
            String key = txtCryptoCode.Text;

            uint[] dataU = GenerateTeaData(data);
            uint[] keyU = GenerateTeaKey(key);

            uint[] encryptedUints = tea.Encrypt(dataU, keyU);
            String encryptedTextA = encryptedUints[0].ToString();
            String encryptedTextB = encryptedUints[1].ToString();


            zService.teaUpload(user, fileName, "tea", encryptedTextA, encryptedTextB, key, size, hashCode);

        }

        protected void teaDownload(String user, String file, String type, String key, int size, GridViewCommandEventArgs e, String hashCode)
        {
            StreamReader readData = new StreamReader(Server.MapPath("~/Data/" + user + "/") + file);
            string encryptedA = readData.ReadLine();
            string encryptedB = readData.ReadLine();
            readData.Close();

            uint[] dataU = new uint[2];
            dataU[0] = Convert.ToUInt32(encryptedA);
            dataU[1] = Convert.ToUInt32(encryptedB);
            uint[] keyU = GenerateTeaKey(key);

            uint[] decryptedUints = tea.Decrypt(dataU, keyU);
            String decryptedText = tea.uintArrayToString(decryptedUints);
            String hashCode2 = HashCode(decryptedText);
            String encrypted = encryptedA + "\r\n" + encryptedB;

            if (hashCode == hashCode2)
            {
                zService.teaDownload(user, file, decryptedText);
                DownloadFile(user, e);
                zService.teaAfterDEncrypt(user, file, encrypted);
            }
            else
                Response.Write("<script>alert('Wrong hash.')</script>");
        }

        protected void elGamalUpload(string user)
        {
            StreamReader readUpFile = new StreamReader(FileUpload1.FileContent);
            string data = readUpFile.ReadToEnd();
            String hashCode = HashCode(data);
            readUpFile.Close();

            FileUpload1.PostedFile.SaveAs(Server.MapPath("~/Data/" + user + "/") + FileUpload1.FileName);

            String fileName = FileUpload1.FileName;
            String key = txtCryptoCode.Text;
            int[] encrypted = elGamal.Encrypt(Convert.ToInt32(data),Convert.ToInt32(key));
            String a = encrypted[0].ToString();
            String b = encrypted[1].ToString();
            int size = FileUpload1.PostedFile.ContentLength;

            zService.elGamalUpload(user, fileName, "elgamal", a, b, size, key, hashCode);

        }

        protected void elGamalDownload(string user, string file, string type, string key, int size, GridViewCommandEventArgs e, string hashCode)
        {
            StreamReader readData = new StreamReader(Server.MapPath("~/Data/" + user + "/") + file);
            int a = Convert.ToInt32(readData.ReadLine());
            int b = Convert.ToInt32(readData.ReadLine());
            readData.Close();

            String decryptedText = elGamal.Decrypt(a, b).ToString();
            String hashCode2 = HashCode(decryptedText);

            if (hashCode == hashCode2)
            {
                zService.elGamalDownload(user, file, decryptedText);
                DownloadFile(user, e);
                zService.elGamalAfterDEncrypt(user, file, a, b);
            }
            else
                Response.Write("<script>alert('Wrong hash.')</script>");
        }

        protected void DownloadFile(String user, GridViewCommandEventArgs e)
        {
            Response.Clear();
            Response.ContentType = "application/octet-stream";
            Response.AppendHeader("content-disposition", "filename=" + e.CommandArgument);
            Response.TransmitFile(Server.MapPath("~/Data/" + user + "/") + e.CommandArgument);
            Response.Flush();
            Response.SuppressContent = true;

        }



        protected String HashCode(String input)
        {
            uint[] hash = { 0x67452301, 0xEFCDAB89, 0x98BADCFE, 0x10325476, 0xC3D2E1F0 };
            byte[] bytes = sha1.padInput(Encoding.ASCII.GetBytes(input));
            uint[] bigpadded = new uint[80];

            uint[] paddedUints = new uint[bytes.Length/4];
            int i = 0;
            int j = 0;
            for(i = 0; i < bytes.Length / 4; i+=4)
            {
                paddedUints[j] = BitConverter.ToUInt32(bytes, i);
                j++;
            }

            sha1.processBlock(paddedUints, hash, bigpadded);
            return sha1.uintArrayToString(hash);
        }

        protected void btnLogout_Click(object sender, EventArgs e)
        {
            Response.Redirect("start.aspx");
        }

    }
}