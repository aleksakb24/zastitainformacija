﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="login.aspx.cs" Inherits="ZastitaInformacija.login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="login.css" rel="stylesheet" type="text/css" />
    <title></title>
</head>
<body style="height: 361px">
    <form id="form1" runat="server">
        <div style="height: 62px">
            <asp:Label ID="Label1" runat="server" Font-Size="X-Large" Text="Sign In" class="borderName"></asp:Label>
        </div>
        <div style="height: 41px">
            <asp:Label ID="Label2" runat="server" Text="Username:"></asp:Label>
&nbsp;&nbsp;&nbsp;
            <asp:TextBox ID="txtUsernameL" runat="server" CssClass="textBox"></asp:TextBox>
        </div>
        <div style="height: 44px">
            <asp:Label ID="Label3" runat="server" Text="Password:"></asp:Label>
&nbsp;&nbsp;&nbsp;
            <asp:TextBox ID="txtPasswordL" runat="server" TextMode="Password" CssClass="textBox"></asp:TextBox>
        </div>
        <div style="height: 46px">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Button ID="btnLogin" runat="server" Text="Login" OnClick="btnLogin_Click" class="button"/>
        </div>
        <div>
            <asp:HyperLink ID="HyperLink1"  NavigateUrl="~/register.aspx" runat="server">Don't have an account? Click here to register.</asp:HyperLink>
        </div>
      

    </form>
</body>
</html>
