﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;

namespace ZastitaInformacija
{
    public partial class login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            foreach (string strFile in Directory.GetFiles(Server.MapPath("~/Users")))
            {
                FileInfo fi = new FileInfo(strFile);

                if (fi.Name == txtUsernameL.Text + ".txt")
                {
                    StreamReader reader = File.OpenText(Server.MapPath("~/Users/" + txtUsernameL.Text + ".txt"));
                    string password = reader.ReadLine();
                    reader.Close();
                    if (txtPasswordL.Text == password)
                    {
                        String str = txtUsernameL.Text;
                        Response.Redirect("index.aspx?user=" + str);
                    }
                    else
                    {
                        Response.Write("<script>alert('Wrong password')</script>");
                        return;
                    }


                }
          
            }
            Response.Write("<script>alert('User not found')</script>");
        }
    }
}