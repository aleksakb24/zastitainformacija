﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="register.aspx.cs" Inherits="ZastitaInformacija.register" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="login.css" rel="stylesheet" type="text/css" />
    <title></title>
</head>
<body style="height: 313px">
    <form id="form1" runat="server">
        <div style="height: 61px">
            <asp:Label ID="Label1" runat="server" Font-Size="X-Large" Text="Sign Up" CssClass="borderName"></asp:Label>
        </div>
        <div style="height: 37px">
            <asp:Label ID="Label2" runat="server" Text="Username"></asp:Label>
            :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:TextBox ID="txtUsernameR" runat="server" style="margin-bottom: 0px" CssClass="textBox"></asp:TextBox>
        </div>
        <div style="height: 37px">
            <asp:Label ID="Label3" runat="server" Text="Password:"></asp:Label>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:TextBox ID="txtPasswordR" runat="server" TextMode="Password" CssClass="textBox"></asp:TextBox>
        </div>
        <div style="height: 51px">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Button ID="btnSignup" runat="server" OnClick="btnSignup_Click" Text="Sign Up" class="button"/>
        </div>
        <div>
            <asp:HyperLink ID="hlLogin"  NavigateUrl="~/login.aspx" runat="server">Already have an account? Click here to login.</asp:HyperLink>
        </div>
    </form>
</body>
</html>
