﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;

namespace ZastitaInformacija
{
    public partial class register : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSignup_Click(object sender, EventArgs e)
        {

            foreach (string strFile in Directory.GetFiles(Server.MapPath("~/Users")))
            {
                FileInfo fi = new FileInfo(strFile);

                if(fi.Name == txtUsernameR.Text + ".txt")
                {
                    Response.Write("<script>alert('User with this username already exists.')</script>");
                    return;
                }
            }

            var dir1 = Server.MapPath("~/Users");
            var file = Path.Combine(dir1, txtUsernameR.Text + ".txt");
            var dir2 = Server.MapPath("~/Data/" + txtUsernameR.Text);
            Directory.CreateDirectory(dir2);
            File.WriteAllText(file, txtPasswordR.Text);
            Response.Redirect("index.aspx?user=" + txtUsernameR.Text);

        }
    }
}