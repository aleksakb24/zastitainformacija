﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="start.aspx.cs" Inherits="ZastitaInformacija.start" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="start.css" rel="stylesheet" type="text/css" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div style="height: 82px">
            <asp:Label ID="Label1" runat="server" Font-Size="XX-Large" Text="Crypto Drive" CssClass="borderName"></asp:Label>
        </div>
        <div style="height: 70px">
            <asp:Button ID="btnLoginS" runat="server" Text="Login" class="button" OnClick="btnLoginS_Click"/>
        </div>
        <div>
            <asp:Button ID="btnRegisterS" runat="server" Text="Sign Up" class="button" OnClick="btnRegisterS_Click"/>
        </div>
    </form>
</body>
</html>
